import {
  ItemMtA
} from "./item.js";


/**
 * Extend the basic ItemSheet with some very simple modifications
 */
export class MtAItemSheet extends ItemSheet {
  constructor(...args) {
    super(...args);
    
    Handlebars.registerHelper('isArcadian', function (value) {
      return value === "Arcadian";
    });
    
    Handlebars.registerHelper('isCourt', function (value) {
      return value === "Court";
    });
    
    Handlebars.registerHelper('isGoblin', function (value) {
      return value === "Goblin";
    });
  }

  /**
   * Extend and override the default options used by the Simple Item Sheet
   * @returns {Object}
   */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
			classes: ["mta-sheet", "sheet", "item"],
      width: 630,
      tabs: [{
        navSelector: ".tabs",
        contentSelector: ".sheet-body",
        initial: "traits"
      }]
		});
  }

  /* -------------------------------------------- */

  /**
   * Return a dynamic reference to the HTML template path used to render this Item Sheet
   * @return {string}
   */
  get template() {
    const path = "systems/mta-umg/templates/items";
    return `${path}/${this.item.data.type}.html`;
  }

  /* -------------------------------------------- */

  /**
   * Prepare data for rendering the Item sheet
   * The prepared data object contains both the actor data as well as additional sheet options
   */
  getData() {
    const data = super.getData();
    
    data.config = CONFIG.MTA;
    const owner = this.actor;
    if(this.item.type === "container"){
      data.data.contents = data.data.contents || [];
      data.inventory = this._getContainerInventory(data);
    } 
    if(owner?.data.data.characterType === "Changeling"){
       data.isChangeling = true; 
       }
    
    console.trace(data);
    return data;
  }

  /* -------------------------------------------- */

  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
	activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;
    
    if (this.item.type === "container") this._registerContainerListeners(html);
  }

  /* -------------------------------------------- */
  /*                  CONTAINERS                  */
  /* -------------------------------------------- */
  
  
  _getContainerInventory(data){
    const inventory = {
      firearm: {
        label: "Firearm",
        items: [],
        dataset: ["Dmg.", "Range", "Cartridge", "Magazine", "Init.", "Size"]
      },
      melee: {
        label: "Melee",
        items: [],
        dataset: ["Damage", "Type", "Initiative", "Size"]
      },
      armor: {
        label: "Armor",
        items: [],
        dataset: ["Rating", "Defense", "Speed", "Coverage"]
      },
      equipment: {
        label: "Equipment",
        items: [],
        dataset: ["Dice bonus", "Durability", "Structure", "Size"]
      },
      ammo: {
        label: "Ammo",
        items: [],
        dataset: ["Cartridge", "Quantity"]
      },
      container: {
        label: "Containers",
        items: [],
        dataset: ["Durability", "Structure", "Size"]
      }
    };

    data.data.contents.forEach(item => {
      if (inventory[item.type]) {
        if (!inventory[item.type].items) {
          inventory[item.type].items = [];
        }
        inventory[item.type].items.push(item);
      }
    });
    return inventory;
  }
  
  
  _registerContainerListeners(html){
    //this.form.ondragover = ev => this._onDragOver(ev);
    this.form.ondrop = ev => this._onDrop(ev);

    html.find('.item-row').each((i, li) => {
      if ( li.classList.contains("header") ) return;
      li.setAttribute("draggable", true);
      li.addEventListener("dragstart", this._onDragItemStart.bind(this), false);
    });
    
    html.find('.cell.item-name span').click(event => this._onItemSummary(event));

    //document.addEventListener("dragend", this._onDragEnd.bind(this));
    
    // Delete Inventory Item
    html.find('.item-delete').click(event => {
      const data = this.getData();
      const index = Number(event.currentTarget.dataset.index);
      let itemList = duplicate(data.data.contents);
      itemList.splice(index, 1);
      this.item.update({["data.contents"]: itemList});
    });
  }
  
  
  _onItemSummary(event) {
    event.preventDefault();
    const data = this.getData();
    let li = $(event.currentTarget).parents(".item-row");
    let index = Number(li.data("index"));
    let item = data.data.contents[index];
    
    let chatData = duplicate(item.data);
    chatData.description = TextEditor.enrichHTML(chatData.description, {secrets: this.owner});
    
    //let chatData = item.getChatData({secrets: this.owner});
    let tb = $(event.currentTarget).parents(".item-table");
    
    let colSpanMax = [...tb.get(0).rows[0].cells].reduce( (a,v) => (v.colSpan)?a+v.colSpan*1:a+1 ,0);
    
    // Toggle summary
    if ( li.hasClass("expanded") ) {
      let summary = li.next(".item-summary");
      summary.children().children("div").slideUp(200, () => summary.remove());
    } else {
      let tr = $(`<tr class="item-summary"> <td colspan="${colSpanMax}"> <div> ${chatData.description} </div> </td> </tr>`);
      //let props = $(`<div class="item-properties"></div>`);
      //chatData.properties.forEach(p => props.append(`<span class="tag">${p}</span>`));
      //div.append(props);
      let div = tr.children().children("div");
      div.hide();
      li.after(tr);
      div.slideDown(200);
    }
    li.toggleClass("expanded");
  }
  
  async _onDragItemStart(event) {
    //event.preventDefault();
    //event.stopPropagation();
    const data = this.getData();
    
    const index = Number(event.currentTarget.dataset.index);
    let item = data.data.contents[index];
    item = duplicate(item);
    
    event.dataTransfer.setData("text/plain", JSON.stringify({
      type: "Item",
      data: item
    }));
  }


  async _onDrop(event) {
    //event.preventDefault();
    //event.stopPropagation();   

    // Try to extract the data
    let data;
    try {
      data = JSON.parse(event.dataTransfer.getData('text/plain'));
    } catch (err) {
      return false;
    }
    
    if(data.type === "Item"){
      const ownData = this.getData();
      const item = await Item.fromDropData(data);
      
      if(item.data.containerID === this.item._id){
        return false;
      }
      
      const newItem = new Item(duplicate(item.data), {temporary: true});
      newItem.data._id = undefined;
      newItem.data.containerID = this.item._id;
      
      let itemList = duplicate(ownData.data.contents);
      itemList.push(newItem.data);

      return this.item.update({["data.contents"]: itemList});
    }
    
    return false;
  }
}
